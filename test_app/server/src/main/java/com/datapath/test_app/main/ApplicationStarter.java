package com.datapath.test_app.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "com.datapath.*" })
public class ApplicationStarter {
	public static void main(String[] args) {
	    SpringApplication.run(ApplicationStarter.class, args);
	}
}

