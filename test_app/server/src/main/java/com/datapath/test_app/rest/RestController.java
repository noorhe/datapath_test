package com.datapath.test_app.rest;

import com.datapath.test_app.pojos.Job;
import com.datapath.test_app.processing.JobProcessor;
import com.datapath.test_app.processing.ThreadPoolWorker;
import com.datapath.test_app.utils.Constants;
import com.datapath.test_app.utils.PluginsLoader;
import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.async.DeferredResult;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@org.springframework.web.bind.annotation.RestController
public class RestController {
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private final List<JobProcessor> jobProcessors = PluginsLoader.loadPlugins(JobProcessor.class);
    private final ThreadPoolWorker threadPoolWorker = new ThreadPoolWorker(jobProcessors);

    @RequestMapping("/submit_job")
    public DeferredResult<ResponseEntity<?>> submitJob(@RequestParam(name="job_type") final String jobTypeName, @RequestParam(name="job_body", required=false) final String jobBody) {
        val result = new DeferredResult<ResponseEntity<?>>();
        try {
            threadPoolWorker
                    .submitJob(new Job(Constants.JOB_TYPE.valueOf(jobTypeName), jobBody))
                    .handle((res, ex) -> {
                        if(res != null){
                            result.setResult(ResponseEntity.ok(res));
                        } else {
                            String commonErrorStr = buildJobFailedString(jobTypeName, jobBody);
                            if(ex instanceof UnsupportedOperationException){
                                LOGGER.error(commonErrorStr + ": UnsupportedOperationException");
                                result.setResult(ResponseEntity.badRequest()
                                        .body("400 (Bad Request): Requested job type not found."));
                            } else {
                                LOGGER.error(commonErrorStr);
                                result.setResult(ResponseEntity.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
                                        .body("500 (Internal server error): " + ex.getMessage()));
                            }
                        }
                        return null;
                    });
        } catch (Throwable t){
            LOGGER.error("Error for jobTypeName = " + jobTypeName + " jobBody = " + jobBody, t);
            throw new RuntimeException(t);
        }
        return result;
    }

    @RequestMapping("/set_threads_number")
    public void setThreadsNumber(@RequestParam(name="threads_number")final int threadsNumber){
        threadPoolWorker.setNumberOfThreads(threadsNumber);
    }

    private String buildJobFailedString(final String jobName, final String jobBody) {
        return "Failed to process job for jobName = " + jobName + " jobBody = " + jobBody;
    }
}