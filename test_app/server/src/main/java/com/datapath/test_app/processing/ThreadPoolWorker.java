package com.datapath.test_app.processing;

import com.datapath.test_app.pojos.Job;
import lombok.val;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class ThreadPoolWorker {
    private final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    private final List<JobProcessor> jobProcessors;
    private final int DEFAULT_POOL_THREADS_NUMBER = 1;
    private final ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(DEFAULT_POOL_THREADS_NUMBER);

    public ThreadPoolWorker(List<JobProcessor> jobProcessors){
        this.jobProcessors = jobProcessors;
    }


    public CompletableFuture<Object> submitJob(Job job){
        CompletableFuture<Object> completableFuture = new CompletableFuture<>();
        executor.submit(() -> {
            try{
                completableFuture.complete(processJob(job));
            } catch (UnsupportedOperationException e){
                LOGGER.error("No processor found for jobType = " + job.getJobType());
                completableFuture.completeExceptionally(e);
            } catch (Throwable t){
                LOGGER.error("Error for params: jobType = " + job.getJobType() + " jobBody = " + job.getJobBody(), t);
                completableFuture.completeExceptionally(t);
            }
        });
        return completableFuture;
    }

    private Object processJob(Job job) {
        return jobProcessors.stream()
                .filter(processor -> processor.isApplicable(job))
                .findFirst()
                .orElseThrow(UnsupportedOperationException::new)
                .process(job);
    }

    public void setNumberOfThreads(int n){
        try {
            executor.setMaximumPoolSize(n);
        } catch (IllegalArgumentException e){
            LOGGER.error("Error while changing number of thread to " + n);
            throw new RuntimeException("Min threads number must be > " + DEFAULT_POOL_THREADS_NUMBER);
        }
    }
}
