package com.datapath.test_app.utils;

import com.datapath.test_app.processing.JobProcessor;

import java.util.ArrayList;

import java.util.List;
import java.util.ServiceLoader;

public class PluginsLoader {
    public static List<JobProcessor> loadPlugins(final Class<JobProcessor> serviceClass) {
        final List<JobProcessor> services = new ArrayList<>();
        for (final JobProcessor service : ServiceLoader.load(serviceClass)) {
            services.add(service);
        }
        return services;
    }
}
