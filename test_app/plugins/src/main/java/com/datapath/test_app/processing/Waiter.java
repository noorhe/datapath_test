package com.datapath.test_app.processing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.datapath.test_app.pojos.Job;
import com.datapath.test_app.utils.Constants;

public class Waiter extends AbstractJobProcessor {
    public static final int TWENTY_SECONDS = 20000;
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Override
    protected String doTheJob(Job job) {
        try {
            Thread.currentThread().sleep(TWENTY_SECONDS);
        } catch (InterruptedException e) {
            LOGGER.error("Waiter interrupted", e);
        }
        return TWENTY_SECONDS + " ms passed";
    }

    @Override
    protected Constants.JOB_TYPE thisJobType() {
        return Constants.JOB_TYPE.WAIT_FOR_20_SECONDS;
    }
}
