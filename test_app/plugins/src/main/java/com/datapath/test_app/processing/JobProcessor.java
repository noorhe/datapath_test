package com.datapath.test_app.processing;

import com.datapath.test_app.pojos.Job;

public interface JobProcessor {
    Object process(Job job);
    boolean isApplicable(Job job);
}
