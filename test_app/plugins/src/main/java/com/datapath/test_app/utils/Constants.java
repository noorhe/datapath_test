package com.datapath.test_app.utils;

public class Constants {
    public enum JOB_TYPE {
        CALCULATE_PI,
        WAIT_FOR_20_SECONDS,
        FAIL_IMMEDIATELY,
        COUNT_WORDS
    }
}
