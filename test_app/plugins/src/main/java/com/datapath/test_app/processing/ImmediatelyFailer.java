package com.datapath.test_app.processing;


import com.datapath.test_app.pojos.Job;
import com.datapath.test_app.utils.Constants;

public class ImmediatelyFailer extends AbstractJobProcessor {
    @Override
    protected Void doTheJob(Job job) {
        throw new RuntimeException("Very meaningful exception occurred.");
    }

    @Override
    protected Constants.JOB_TYPE thisJobType() {
        return Constants.JOB_TYPE.FAIL_IMMEDIATELY;
    }
}
