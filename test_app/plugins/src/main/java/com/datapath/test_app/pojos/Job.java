package com.datapath.test_app.pojos;

import lombok.Data;
import com.datapath.test_app.utils.Constants;

@Data
public class Job {
    final private Constants.JOB_TYPE jobType;
    final private String jobBody;
}