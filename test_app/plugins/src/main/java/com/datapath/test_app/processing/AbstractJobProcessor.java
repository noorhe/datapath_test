package com.datapath.test_app.processing;

import com.datapath.test_app.pojos.Job;
import com.datapath.test_app.utils.Constants;

abstract public class AbstractJobProcessor implements JobProcessor {
    @Override
    public final boolean isApplicable(Job job) {
        return job.getJobType().equals(thisJobType());
    }

    @Override
    public final Object process(Job job) {
        if(isApplicable(job)){
            return doTheJob(job);
        } else {
            throw new UnsupportedOperationException(this.getClass().getCanonicalName() + ": " + job.toString());
        }
    }

    protected abstract Object doTheJob(Job job);
    protected abstract Constants.JOB_TYPE thisJobType();
}
