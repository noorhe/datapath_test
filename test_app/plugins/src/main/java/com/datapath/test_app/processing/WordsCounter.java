package com.datapath.test_app.processing;


import com.datapath.test_app.pojos.Job;
import com.datapath.test_app.utils.Constants;

public class WordsCounter extends AbstractJobProcessor {
    @Override
    protected Integer doTheJob(Job job) {
        if(job.getJobBody() == null){
            throw new IllegalArgumentException("No valid string to count words in");
        }
        final String jobStr = job.getJobBody().trim();
        return jobStr.isEmpty() ? 0 : job.getJobBody().split(" ").length;
    }

    @Override
    protected Constants.JOB_TYPE thisJobType() {
        return Constants.JOB_TYPE.COUNT_WORDS;
    }
}
