package com.datapath.test_app.processing;

import com.datapath.test_app.pojos.Job;
import com.datapath.test_app.utils.Constants;

public class PiCalculator extends AbstractJobProcessor{
    @Override
    protected Double doTheJob(Job job) {
        return Math.PI;
    }

    @Override
    protected Constants.JOB_TYPE thisJobType() {
        return Constants.JOB_TYPE.CALCULATE_PI;
    }
}
